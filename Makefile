# Makefile to simplify some common build and deployment operations.

# Find our exact Python 3 version. Override with PYTHONBIN variable before
# calling if you want an alternate Python.
PYTHONBIN ?= $(shell python3-config --prefix)/bin/python3

PYVER := $(shell $(PYTHONBIN) -c 'import sys;print("{}.{}".format(sys.version_info[0], sys.version_info[1]))')
ABIFLAGS := $(shell $(PYTHONBIN)-config --abiflags)
SUFFIX := $(shell $(PYTHONBIN)-config --extension-suffix)

export PYVER

GPG = gpg2

.PHONY: info build install clean distclean cleandist develop sdist bdist sign publish

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  info          Show info about the Python being used."
	@echo "  build         to just build the packages."
	@echo "  install       to install from this workspace."
	@echo "  develop       to set up for local development."
	@echo "  clean         to clean up build artifacts."
	@echo "  distclean     to make source tree pristine."
	@echo "  sdist         to build source distribution."
	@echo "  bdist         to build binary distribution (wheel)."
	@echo "  publish       to push to PyPI."

info:
	@echo Found Python version: $(PYVER)$(ABIFLAGS)
	@echo Specific Python used: $(PYTHONBIN)
	@echo Python exension suffix: $(SUFFIX)

build:
	$(PYTHONBIN) setup.py build

install: build
	$(PYTHONBIN) setup.py install --skip-build --optimize=2

develop:
	$(PYTHONBIN) setup.py develop --user

clean:
	$(PYTHONBIN) setup.py clean
	find . -depth -type d -name __pycache__ -exec rm -rf {} \;

distclean: clean
	make -C docs clean
	rm -rf pywin32compat.egg-info
	rm -rf dist
	rm -rf build
	rm -rf .cache
	rm -rf .eggs

cleandist:
	rm -f dist/*

sdist:
	$(PYTHONBIN) setup.py sdist

bdist:
	$(PYTHONBIN) setup.py bdist_wheel

sign: cleandist bdist sdist
	$(GPG) --detach-sign -a dist/pywin32compat-*.whl
	$(GPG) --detach-sign -a dist/pywin32compat-*.tar.gz

publish: sign
	$(PYTHONBIN) -m twine upload -r ouster dist/*

