#!/usr/bin/env python3.7

"""Setup for win32compat package.
"""

import os
from glob import glob
from setuptools import setup

NAME = "pywin32compat"
VERSION = "1.0.0"

with open('README.md') as fo:
    LONG_DESCRIPTION = fo.read()

MODULES = [os.path.splitext(f)[0] for f in glob("win*.py")]

setup(
    name=NAME,
    version=VERSION,
    py_modules=MODULES,
    description="Compatibility modules to make pywin32-using modules importable on Linux.",
    long_description_content_type="text/markdown",
    long_description=LONG_DESCRIPTION,
    license="MIT",
    author="Keith Dart",
    author_email="keith@dartworks.biz",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Operating System :: POSIX",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
