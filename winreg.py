"""Stub module to make imports on Linux not complain."""


class WindowsError(Exception):
    pass


error = WindowsError


KEY_ALL_ACCESS = 0
KEY_WRITE = 0
KEY_READ = 0
KEY_EXECUTE = 0
KEY_QUERY_VALUE = 0
KEY_SET_VALUE = 0
KEY_CREATE_SUB_KEY = 0
KEY_ENUMERATE_SUB_KEYS = 0
KEY_NOTIFY = 0
KEY_CREATE_LINK = 0
KEY_WOW64_64KEY = 0
KEY_WOW64_32KEY = 0
REG_BINARY = 0
REG_DWORD = 0
REG_DWORD_LITTLE_ENDIAN = 0
REG_DWORD_BIG_ENDIAN = 0
REG_EXPAND_SZ = 0
REG_LINK = 0
REG_MULTI_SZ = 0
REG_NONE = 0
REG_QWORD = 0
REG_QWORD_LITTLE_ENDIAN = 0
REG_RESOURCE_LIST = 0
REG_FULL_RESOURCE_DESCRIPTOR = 0
REG_RESOURCE_REQUIREMENTS_LIST = 0
REG_SZ = 0

HKEY_CLASSES_ROOT = 0
HKEY_CURRENT_USER = 0
HKEY_LOCAL_MACHINE = 0
HKEY_USERS = 0
HKEY_PERFORMANCE_DATA = 0
HKEY_CURRENT_CONFIG = 0
HKEY_DYN_DATA = 0


class PyHKEY:

    def Close(self):
        pass

    def Detach(self):
        pass


def CloseKey(hkey):
    pass


def ConnectRegistry(computer_name, key):
    pass


def CreateKey(key, sub_key):
    pass


def CreateKeyEx(key, sub_key, reserved=0, access=KEY_WRITE):
    pass


def DeleteKey(key, sub_key):
    pass


def DeleteKeyEx(key, sub_key, access=KEY_WOW64_64KEY, reserved=0):
    pass


def DeleteValue(key, value):
    pass


def EnumKey(key, index):
    raise OSError("no key for you")


def EnumValue(key, index):
    pass


def ExpandEnvironmentStrings(str):
    pass


def FlushKey(key):
    pass


def LoadKey(key, sub_key, file_name):
    pass


class OpenKey:
    def __init__(self, key, sub_key, reserved=0, access=KEY_READ):
        pass

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        pass


def OpenKeyEx(key, sub_key, reserved=0, access=KEY_READ):
    pass


def QueryInfoKey(key):
    pass


def QueryValue(key, sub_key):
    pass


def QueryValueEx(key, value_name):
    raise OSError("no key")


def SaveKey(key, file_name):
    pass


def SetValue(key, sub_key, type, value):
    pass


def SetValueEx(key, value_name, reserved, type, value):
    pass


def DisableReflectionKey(key):
    pass


def EnableReflectionKey(key):
    pass


def QueryReflectionKey(key):
    pass
