# Win32 Compatibility Modules

This package contains skeletal modules that mimic pywin32 modules. This should
enable other modules that import these to be importable on Linux without
getting import errors. Nothing else will work, but at least you won't get
import errors. This enables extracting the documentation strings.

